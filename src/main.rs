#![no_std]
#![no_main]

// pick a panicking behavior
extern crate panic_halt; // you can put a breakpoint on `rust_begin_unwind` to catch panics
// extern crate panic_abort; // requires nightly
// extern crate panic_itm; // logs messages over ITM; requires ITM support
// extern crate panic_semihosting; // logs messages to the host stderr; requires a debugger

//use cortex_m::asm;
use cortex_m_rt::entry;
use cortex_m_semihosting::{debug, hprintln};

//crypto;
extern crate wyhash;
use wyhash::wyhash;
use wyhash::wyrng;

extern crate core;
use core::default::Default;

#[entry]
fn main() -> ! {
//    asm::nop(); // To not have main optimize to abort in release mode, remove when you add code

    let data = "I have grown to love secrecy. \
                It seems to be the one thing that can make modern life mysterious or marvelous to us. \
                The commonest thing is delightful if only one hides it.".as_bytes();

    let mut seed = 1337;

    let wots = Wots::new(16, &mut seed).unwrap();
    let sign = wots.sign(&data).unwrap();
    let verify = sign.verify(&data);

    hprintln!("Signature: {:?}", verify).unwrap();

    debug::exit(debug::EXIT_SUCCESS);

    loop {
        // your code goes here
    }
}

// STRUCTS

struct Wots<'a> {
    w: usize,
    s_key: SecretKey<'a>,
}

#[derive(Default)]
struct WotsSign {
    sign: [u64;18],
    p_key: [u64;18],
}

struct SecretKey<'a> {
    seed: &'a mut u64,
    s_key: [u64;18],
}

//enum CONST16 {
//    LOG2W = 4,
//    N = 64,
//    L = 18,
//    L1 = 16,
//    L2 = 2,
//}

impl WotsSign {
    fn verify(&self, data: &[u8]) -> bool {
        //hprintln!("Verifying signature");
        let mut concat: [u8;18] = [0;18];
        let digest = wyhash(&data, 0).to_ne_bytes();
        //hprintln!("Hashing data: {:?}", digest);
        let base16 = to_base16(&digest);
        let cs = checksum(&base16);
        
        for (i,v) in base16.iter().chain(&cs).enumerate() {
            concat[i] = *v;
        }
        //hprintln!("Converting to base 16: {:?}", base16);
        let mut verify: [u64;18] = [0;18];
        
        for (i,it) in concat.iter().enumerate() {
            verify[i] = chain_digest(&self.sign[i].to_ne_bytes(), (16-*it) as usize);
        }

        //hprintln!("Generated verifying vector: {:?}", verify);
        let mut result = true;
        for (s, v) in self.p_key.iter().zip(verify.iter_mut()) {
            result &= v==s;
        }
        //hprintln!("Signature: {:?}", result);
        result

    }
}

impl Wots<'_> {
    fn new<'a>(w: usize, seed: &'a mut u64) -> Result<Wots<'a>, &'static str> {
        //hprintln!("Creating new Wots structure");
        let s_key = SecretKey::new(seed).unwrap();
        Ok( Wots{ w, s_key } )
    }

    fn sign(&self, data: &[u8]) -> Result<WotsSign, &'static str> {
        //hprintln!("Signing data");
        let mut concat: [u8;18] = [0;18];

        let p_key = self.gen_pkey();
        let mut sign: [u64;18] = [0;18];

        let digest = wyhash(&data, 0).to_ne_bytes();
        //hprintln!("Hashing data: {:?}", digest);
        let base16 = to_base16(&digest);
        //let base162 = to_base162(&digest);
        //hprintln!("Converting to base 16: {:?}", base16);

        let cs = checksum(&base16);
        
        for (i,v) in base16.iter().chain(&cs).enumerate() {
            concat[i] = *v;
        }

        for (i,it) in concat.iter().enumerate() {
            if *it > 0 {
                sign[i] = chain_digest(&self.s_key.s_key[i].to_ne_bytes(), *it as usize);
            } else {
                sign[i] = self.s_key.s_key[i];
            }
        }
        //hprintln!("Generated signature {:?}", sign);
        Ok( WotsSign { sign, p_key } )
    }

    fn gen_pkey(&self) -> [u64;18] {
        //hprintln!("Generating new public key");

        let mut out: [u64;18] = [0;18];

        for i in 0..18 {
            out[i] = chain_digest(&self.s_key.s_key[i].to_ne_bytes(), 16);
        }
        //hprintln!("Generated new public key: {:?}", out);
        out
    }
}

impl SecretKey<'_> {
    fn new(seed: &mut u64) -> Result<SecretKey, &'static str> {
        //hprintln!("Creating new secret key with seed: {:?}", seed);
        let mut out: [u64;18] = [0;18];
        
        for i in 0..18 {
            out[i] = wyrng(seed);
        }
        //hprintln!("Secret Key: {:?}", out);
        Ok( SecretKey{ seed, s_key:out } )
    }
}

fn chain_digest(data: &[u8], it: usize) -> u64 {
    let mut hash = wyhash(&data, 0);
    for _ in 1..it {
        hash = wyhash(&hash.to_ne_bytes(), 0);
    }
    hash
}

fn to_base162(data: &[u8]) -> [u8;16] {
    let mut out: [u8;16] = [0;16];

    let mut it = 0;
    for i in data.iter() {
        out[it] = (i << 4) >> 4;
        out[it+1] = i >> 4;

        it += 2;
    }
    out
}

fn to_base16(data: &[u8]) -> [u8;16] {
    let mut out: [u8;16] = [0;16];

    let mut it = 0;
    for i in data.iter() {
        out[it] = (i << 4) >> 4;
        out[it+1] = i >> 4;

        it += 2;
    }
    out
}

fn checksum(data: &[u8;16]) -> [u8;2] {
    let mut out: [u8;2] = [0;2];
    let mut aux = 0;

    for i in data {
        aux += i;
    }
    out[0] = (aux << 4) >> 4;
    out[1] = aux >> 4;
    hprintln!("{:?}", out);
    out
}
















